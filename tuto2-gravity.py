import pygame
import random
import math

DRAG = 0.9999
ELASTICITY = 0.75
GRAVITY = (math.pi, 0.002)

background_colour = (0,0,0)
(width, height) = (800, 600)

def addVectors(v1, v2):
    """ Returns the sum of two vectors """
    angle1, length1 = v1
    angle2, length2 = v2
    x  = math.sin(angle1) * length1 + math.sin(angle2) * length2
    y  = math.cos(angle1) * length1 + math.cos(angle2) * length2
    
    angle = 0.5 * math.pi - math.atan2(y, x)
    length  = math.hypot(x, y)

    return (angle, length)

def findParticle(particles, x, y):
    """Returns the first particule that match the (x,y) coordinate or None"""
    for p in particles:
        if math.hypot(p.x-x, p.y-y) <= p.size:
            return p
    return None

def draw_arrow(screen, colour, start, end, thickness=2, size=25, ang=math.radians(160)):
    pygame.draw.aaline(screen,colour,start,end,thickness)
    rotation = math.atan2(start[1]-end[1], end[0]-start[0]) + math.pi/2
    pygame.draw.aaline(screen,colour,end,(end[0]+size*math.sin(rotation-ang), end[1]+size*math.cos(rotation-ang)),thickness)
    pygame.draw.aaline(screen,colour,end,(end[0]+size*math.sin(rotation+ang), end[1]+size*math.cos(rotation+ang)),thickness)
    
class Particle():
    def __init__(self, vect, size, mass=1):
        x, y = vect
        self.x = x
        self.y = y
        self.size = size
        self.colour = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.thickness = 1
        self.speed = random.random()
        self.angle = random.uniform(0, math.pi*2)

    def display(self):
        pygame.draw.circle(screen, self.colour, (int(round(self.x)), int(round(self.y))), self.size, self.thickness)

    def move(self):
        (self.angle, self.speed) = addVectors((self.angle, self.speed), GRAVITY)
        self.x += math.sin(self.angle) * self.speed
        self.y -= math.cos(self.angle) * self.speed
        self.speed *= DRAG

    def bounce(self):
        if self.x > width - self.size:
            self.x = 2*(width - self.size) - self.x
            self.angle = - self.angle
            self.speed *= ELASTICITY

        elif self.x < self.size:
            self.x = 2*self.size - self.x
            self.angle = - self.angle
            self.speed *= ELASTICITY

        if self.y > height - self.size:
            self.y = 2*(height - self.size) - self.y
            self.angle = math.pi - self.angle
            self.speed *= ELASTICITY

        elif self.y < self.size:
            self.y = 2*self.size - self.y
            self.angle = math.pi - self.angle
            self.speed *= ELASTICITY

screen = pygame.display.set_mode((width, height))
pygame.display.set_caption('Gravité')

number_of_particles = 5
my_particles = []

for n in range(number_of_particles):
    size = random.randint(10, 20)
    x = random.randint(size, width-size)
    y = 20

    particle = Particle((x, y), size)

    my_particles.append(particle)

running = True
selected_particle = None
while running:
    screen.fill(background_colour)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            mouseX, mouseY = pygame.mouse.get_pos()
            selected_particle = findParticle(my_particles, mouseX, mouseY)
            if selected_particle:
                selected_particle.thickness = 0
        elif event.type == pygame.MOUSEBUTTONUP:
            selected_particle = None
            for particle in my_particles:
                particle.thickness = 1

    for particle in my_particles:
        if particle != selected_particle:
            particle.move()
            particle.bounce()
        if selected_particle:
            mouseX, mouseY = pygame.mouse.get_pos()
            #pygame.draw.aaline(screen, selected_particle.colour, (selected_particle.x, selected_particle.y), (mouseX, mouseY), 2)
            draw_arrow(screen, selected_particle.colour, (selected_particle.x, selected_particle.y), (mouseX, mouseY))
            dx = mouseX - selected_particle.x
            dy = mouseY - selected_particle.y
            selected_particle.angle = math.atan2(dy, dx) + 0.5*math.pi
            selected_particle.speed = math.hypot(dx, dy) * 0.1
        particle.display()

    pygame.display.flip()