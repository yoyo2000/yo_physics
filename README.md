## Description

Série de scripts python basés sur Pygame et directement inspirés de http://www.petercollingridge.co.uk/pygame-physics-simulation

**Usage :**
```shell
python3 tuto1-bulles.py

python3 tuto2-gravity.py
```

## Installation

Le moteur de jeu [Pygame](http://www.pygame.org/wiki/GettingStarted) est nécessaire.

Pour l'installer : 
```shell
sudo apt install python3-pygame
```

Testé sur Ubuntu.


## License

WTFPL : http://www.wtfpl.net/
