import pygame, random, math

DRAG = 0.9995

class Particle:
    def __init__(self, coords, size):
        x, y = coords
        self.x = x
        self.y = y
        self.size = size
        self.colour = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        self.thickness = 1
        self.speed = random.random()
        self.angle = random.uniform(0, math.pi*2)

    def display(self):
        pygame.draw.circle(screen, self.colour, (int(round(self.x)), int(round(self.y))), self.size, self.thickness)

    def move(self):
        self.speed *= DRAG
        self.x += math.sin(self.angle) * self.speed
        self.y += math.cos(self.angle) * self.speed

    def bounce(self):
        if self.x > width - self.size:
            self.x = 2*(width - self.size) - self.x
            self.angle = - self.angle
        elif self.x < self.size:
            self.x = 2*self.size - self.x
            self.angle = - self.angle
        if self.y > height - self.size:
            self.y = 2*(height - self.size) - self.y
            self.angle = math.pi - self.angle
        elif self.y < self.size:
            self.y = 2*self.size - self.y
            self.angle = math.pi - self.angle

background_colour = (0,0,0)
(width, height) = (800, 600)

screen = pygame.display.set_mode((width, height))
pygame.display.set_caption('Bulles')
screen.fill(background_colour)
# _________________________________

number_of_particles = 50
my_particles = []

for n in range(number_of_particles):
    size = random.randint(10, 20)
    x = random.randint(size, width-size)
    y = random.randint(size, height-size)
    my_particles.append(Particle((x, y), size))

# _________________________________

running = True
while running:
    screen.fill(background_colour)
    for p in my_particles:
        p.move()
        p.bounce()
        p.display()
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False